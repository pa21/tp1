package com.example.suma;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class Calculadora extends AppCompatActivity {

    ArrayList<String> elements = new ArrayList<String>();
    ArrayList<String> operators = new ArrayList<String>();
    Integer[] priorities = {3,3,2,2};
    String[] specials = {"=", "b"};
    boolean isEmpty = true;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calculadora);
        operators.add("*");
        operators.add("/");
        operators.add("+");
        operators.add("-");
        elements.add("");
    }

    private void ReversePolishNotation(){
        ArrayList<String> output = new ArrayList<String>();
        ArrayList<String> stack = new ArrayList<String>();
        ArrayList<String> temp = new ArrayList<String>();
        for (int i = elements.size() - 1; i >=0; i--) {
            if(this.operators.contains(elements.get(i))){
                stack.add(elements.get(i));
                for(int j = 0; j < stack.size(); j++){
                    if( priorities[ operators.indexOf( stack.get(j) ) ] > priorities[ operators.indexOf( elements.get(i) ) ] ){
                        temp.add(stack.get(j));
                        stack.remove(j);
                    }
                    for(int k = 0; k < temp.size(); k++){
                        output.add(temp.get(k));
                    }
                    temp.clear();
                }
            }
            else{
                output.add(elements.get(i));
            }
        }
        for(int i = stack.size()-1; i >= 0; i--){
            output.add(stack.get(i));
        }
        elements.clear();
        elements = (ArrayList)output.clone();
    }

    private void resolve(){
        Double value = null;
        String resp = "";
        try{
            for(int i = 0; i < elements.size(); i++){
                if( operators.contains( elements.get(i) )){
                    switch (elements.get(i)){
                        case "*":
                            value = Double.parseDouble(elements.get(i-1)) * Double.parseDouble(elements.get(i-2));
                            break;
                        case "/":
                            value = Double.parseDouble(elements.get(i-1)) / Double.parseDouble(elements.get(i-2));
                            break;
                        case "+":
                            value = Double.parseDouble(elements.get(i-1)) + Double.parseDouble(elements.get(i-2));
                            break;
                        case "-":
                            value = Double.parseDouble(elements.get(i-1)) - Double.parseDouble(elements.get(i-2));
                            break;
                    }
                    // Parche para corregir lo del .0
                    Double aux = Math.floor(value);
                    String strValue = value.compareTo(aux) == 0 ? aux.toString().replace(".0", "") : value.toString();
                    elements.set(i-2, strValue);
                    elements.remove(i-1);
                    elements.remove(i-1);
                    i=0;
                }
            }
        }
        catch(Exception e){
            System.out.println(e.toString());
            elements.clear();
            elements.add("ERROR");
            isEmpty = true;
        }
    }

    private void addElement(String element){
        if(this.operators.contains(element)){
            this.elements.add(element);
            this.elements.add("");
        }
        else{
            int lastPos = this.elements.size()-1;
            if(isEmpty){
                this.elements.set( lastPos, element ); //Concateno el dato
            }
            else{
                this.elements.set( lastPos, this.elements.get(lastPos) + element ); //Concateno el dato
            }
        }
        isEmpty = false;
    }

    private void deleteElement(){
        /*int index = elements.size() - 1;
        if( elements.get(index) == "" ){
            elements.remove(index);
            elements.remove(index-1);
        }
        else {
            elements.set(index, "");
        }
        La consigna dice que al borrar hay que colocar un 0 -> Limpio el Array y Agrego el 0.
        */
        elements.clear();
        elements.add("0");
        isEmpty = true;
    }

    public void onClick(View view){
        String element = view.getTag().toString();
        if( element.compareTo("b") == 0 ){
            this.deleteElement();
        }
        else if(element.compareTo("=") == 0){
            this.ReversePolishNotation();
            this.resolve();
        }
        else{
            this.addElement(element);
        }
        this.showValue();
    }

    private void showValue(){
        TextView screen = (TextView)findViewById(R.id.txt_resultado);
        String value = "";
        for (int i = 0; i < elements.size(); i++) {
            value += elements.get(i);
        }

        screen.setText(value);
    }
}
