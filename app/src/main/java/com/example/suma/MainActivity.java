package com.example.suma;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText nro1;
    private EditText nro2;
    private TextView res;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.setTitle(R.string.app_name_ej1);

        nro1 = (EditText)findViewById(R.id.EditText1);
        nro2 = (EditText)findViewById(R.id.EditText2);
        res = (TextView)findViewById(R.id.resultado);
    }

    public void Sumar(View view){
        String v1 = nro1.getText().toString();
        String v2 = nro2.getText().toString();

        int n1 = Integer.parseInt(v1);
        int n2 = Integer.parseInt(v2);
        int resultado = n1 + n2;

        res.setText(String.valueOf(resultado));
    }

}