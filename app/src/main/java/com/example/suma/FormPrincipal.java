package com.example.suma;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class FormPrincipal extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_principal);
    }

    public void IrEj1(View view){
        Intent Ej1 = new Intent(this,MainActivity.class);
        startActivity(Ej1);
    }

    public void IrEj2(View view){
        //Intent Ej2 = new Intent(this, Ejection2.class);
        Intent Ej2 = new Intent(this, Calculadora.class);
        startActivity(Ej2);
    }
}